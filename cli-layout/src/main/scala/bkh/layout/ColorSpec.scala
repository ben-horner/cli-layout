package bkh.layout

/**
  *
  */
case class ColorSpec(fg: Option[ScopedColor] = None, bg: Option[ScopedColor] = None) {

  def colorize(string: String): String = {
    val fgPrefix = fg.map(sc => AnsiColors.startFG(sc.ansiColor)).getOrElse("")
    val bgPrefix = bg.map(sc => AnsiColors.startBG(sc.ansiColor)).getOrElse("")
    val suffix = if (fg.isDefined || bg.isDefined) AnsiColors.endColor() else ""
    fgPrefix + bgPrefix + string + suffix
  }

  def combine(parent: ColorSpec): ColorSpec = {
    val newFg = if (this.fg.isEmpty || (parent.fg.isDefined && parent.fg.get.overwrite)) {
      parent.fg // parent provides default, or overwrites
    } else {
      this.fg
    }
    val newBg = if (this.bg.isEmpty || (parent.bg.isDefined && parent.bg.get.overwrite)) {
      parent.bg // parent provides default, or overwrites
    } else {
      this.bg
    }
    ColorSpec(newFg, newBg)
  }

  def combine(parent: Option[ColorSpec]): ColorSpec = {
    if (parent.isDefined) {
      combine(parent.get)
    } else {
      this
    }
  }

}
