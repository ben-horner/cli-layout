package bkh.layout

/**
 * Created by benh on 11/18/15.
 */
// This is just a quick subset.  There are other ansi color definition strings than the simple ones used here.
// The more complex versions didn't work on my mac terminal, but would allow full 255x255x255 RGB values.
// I've only included convenience methods for the single int color values that translate from 6x6x6 RGB values.
// There are also 24 gray scale values, and 16 specific color values that are distinct (run main to see demo).
// This page is unnecessarily complex right now, but the info is there:  https://en.wikipedia.org/wiki/ANSI_escape_code
object AnsiColors {

  val BLACK = rgb(0,0,0)
  val WHITE = rgb(5,5,5)

  val RED = rgb(5,0,0)
  val GREEN = rgb(0,4,0)
  val BLUE = rgb(0,0,5)

  val YELLOW = rgb(5,5,0)
  val MAGENTA = rgb(5,0,5)
  val CYAN = rgb(0,5,5)

  val PURPLE = rgb(2,0,5)
  val ORANGE = rgb(5,2,0)


  def colorize(s: String, fore: Int, back: Int): String = {
    startFG(fore) + startBG(back) + s + endColor()
  }

  def rgb(r: Int, g: Int, b: Int): Int = {
    require(0 <= r && r < 6)
    require(0 <= g && g < 6)
    require(0 <= b && b < 6)
    16 + 36 * r + 6 * g + b
  }

  def startFG(c: Int): String = {
    require(0 <= c && c <256)
    "\u001B[38;5;" + c + "m"
  }

  def startBG(c: Int): String = {
    require(0 <= c && c <256)
    "\u001B[48;5;" + c + "m"
  }

  def endColor(): String = {
    "\u001B[m"
  }

  def main(args: Array[String]): Unit = {
    println("[If you see nothing your terminal doesn't support them]")

    println("Here are the gray values:")
    println("230+                240+                250+")
    println("2 4 4 5 6 7 8 9 0 1 2 4 4 5 6 7 8 9 0 1 2 3 4 5")
    println(((rgb(5,5,5)+1) to 255).map(c => colorize("  ", BLACK, c)).mkString)

    println("Here are the special colors:")
    println("0+                  10+")
    println("0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5")
    println((0 to 16).map(c => (colorize("  ", BLACK, c))).mkString)

    println("Here are all the RGB values (3D, so showing red layers):")
    println("R-> 0           1           2           3           4           5")
    println("G---v")
    println("B-v 0 1 2 3 4 5 0 1 2 3 4 5 0 1 2 3 4 5 0 1 2 3 4 5 0 1 2 3 4 5 0 1 2 3 4 5")
    (0 to 5).foreach{ b =>
      print("  " + b)
      (0 to 5).foreach{ g =>
        (0 to 5).foreach{ r =>
          print(colorize("  ", BLACK, rgb(r, g, b)))
        }
      }
      println()
    }
  }

}
