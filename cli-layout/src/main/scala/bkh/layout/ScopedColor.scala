package bkh.layout

/**
  *
  */
case class ScopedColor(ansiColor: Int, overwrite: Boolean = false) {

  def combine(parent: ScopedColor): ScopedColor = {
    if (parent.overwrite) {
      parent
    } else {
      this
    }
  }

  def combine(parent: Option[ScopedColor]): ScopedColor = {
    if (parent.isDefined) {
      combine(parent.get)
    } else {
      this
    }
  }

}
