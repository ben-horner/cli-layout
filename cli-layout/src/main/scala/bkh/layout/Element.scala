package bkh.layout

/**
  * The purpose of this class is to let you build up a grid of characters for display on the console easily.
  * So that you can draw a representation of a game board, or the current thought process of a player or whatever.
  *
  * An element can have immediate contents, or an element can combine other elements
  *
  * The elements need to remain unevaluated so that additional attributes can be added (such as color).
  * The problem is that to achieve the colors, we have to add prefix and suffix strings which mess with the widths.
  *
  * The toString method turns an element into a string
  *
  * Created by ben on 7/22/17.
  */



sealed trait Element {
  def colors: ColorSpec
  def width: Int
  def height: Int
  def render: Seq[String]

  def withColors(colors: ColorSpec): Element

  // only increases width
  def widen(width: Int, alignment: HorizontalAlignment = Alignment.Center): Element = {
    if (this.width >= width) this
    else alignment match {
      case Alignment.Left => this.leftOf(Element.fill(width - this.width, height, ' ', colors))
      case Alignment.Center => {
        // width - this.width == 1
        val left = Element.fill((width - this.width) / 2, height, ' ', colors)
        val right = Element.fill(width - this.width - left.width, height, ' ', colors)
        left.leftOf(this).leftOf(right)
      }
      case Alignment.Right => Element.fill(width - this.width, height, ' ', colors).leftOf(this)
    }
  }

  // only increases height
  def heighten(height: Int, alignment: VerticalAlignment = Alignment.Center): Element = {
    if (this.height >= height) this
    else alignment match {
      case Alignment.Top => this.above(Element.fill(width, height - this.height, ' ', colors))
      case Alignment.Center => {
        // height - this.height == 1
        val top = Element.fill(width, (height - this.height) / 2, ' ', colors)
        val bottom = Element.fill(width, height - this.height - top.height, ' ', colors)
        top.above(this).above(bottom)
      }
      case Alignment.Bottom => Element.fill(width, height - this.height, ' ', colors).above(this)
    }
  }

  def leftOf(that: Element, alignment: VerticalAlignment = Alignment.Center): Element =
    RowElement(Seq(this, that), alignment)

  def rightOf(that: Element, alignment: VerticalAlignment = Alignment.Center): Element =
    that.leftOf(this)

  def beside(that: Element, alignment: VerticalAlignment = Alignment.Center): Element =
    this.leftOf(that)

  def above(that: Element, alignment: HorizontalAlignment = Alignment.Center): Element =
    ColElement(Seq(this, that), alignment)

  def below(that: Element, alignment: HorizontalAlignment = Alignment.Center): Element =
    that.above(this, alignment)

  def boxed: Element = {
    val corner = Element.fill(1, 1, '+', colors)
    val horiz = Element.fill(width, 1, '-', colors)
    val vert = Element.fill(1, height, '|', colors)
    val end = corner.above(vert).above(corner)
    val middle = horiz.above(this).above(horiz)
    end.leftOf(middle).leftOf(end)
  }
}

object Element {
  def apply(contents: Seq[String], alignment: HorizontalAlignment): Element = {
    new ColElement(contents.map(BasicElement(_)), alignment)
  }

  def apply(contents: String, alignment: HorizontalAlignment = Alignment.Center): Element = {
    this(contents.split('\n').toIndexedSeq, alignment)
  }

  def row(row: Seq[Element], alignment: VerticalAlignment = Alignment.Center): Element = {
    if (row.isEmpty) DegenerateElement(0,0)
    else RowElement(row, alignment)
  }

  def col(col: Seq[Element], alignment: HorizontalAlignment = Alignment.Center): Element = {
    if (col.isEmpty) DegenerateElement(0,0)
    else ColElement(col, alignment)
  }

  def fill(width: Int,
           height: Int,
           chr: Char,
           colors: ColorSpec = ColorSpec(None, None)): Element = {
    if ((width == 0) || (height == 0)) DegenerateElement(width, height, colors)
    else {
      val cell = BasicElement(chr.toString * width, colors)
      fillCol(height, cell)
    }
  }

  def fillRow(size: Int, element: Element): Element = {
    row((1 to size).map(_ => element))
  }

  def fillCol(size: Int, element: Element): Element = {
    col((1 to size).map(_ => element))
  }

}

case class DegenerateElement(width: Int, height: Int, colors: ColorSpec = ColorSpec(None, None)) extends Element {
  require((width == 0) || (height == 0))

  override def widen(width: Int, alignment: HorizontalAlignment = Alignment.Center): Element = {
    if (width <= this.width) this
    else if (height == 0) copy(width = width)
    else Element.fill(width, height, ' ', colors)
  }

  override def heighten(height: Int, alignment: VerticalAlignment = Alignment.Center): Element = {
    if (height <= this.height) this
    else if (width == 0) copy(height = height)
    else Element.fill(width, height, ' ', colors)
  }

  override def render: Seq[String] = {
    if (height > 0) Seq.fill(height)("")
    else Seq()
  }

  override def withColors(colors: ColorSpec): Element = copy(colors = colors)
}

case class BasicElement(contents: String, colors: ColorSpec = ColorSpec(None, None)) extends Element {
  require(contents.nonEmpty)
  require(contents.length > 0)
  require(!contents.contains('\n'))
  override def width: Int = contents.length
  override def height: Int = 1
  override def render: Seq[String] = Seq(colors.colorize(contents))
  override def withColors(colors: ColorSpec): Element = copy(colors = colors)
}

case class ColElement(col: Seq[Element],
                      alignment: HorizontalAlignment = Alignment.Center,
                      colors: ColorSpec = ColorSpec(None, None)) extends Element {
  require(col.nonEmpty, "ColElement(Seq()) called, arg must be non-empty, try Element.col() to handle automatically")
  override val width = col.map(_.width).max
  override val height = col.map(_.height).sum
  override def withColors(colors: ColorSpec): Element = copy(colors = colors)
  override def render: Seq[String] = {
    col.flatMap{ elt =>
      val renderColors = elt.colors.combine(colors)
      elt.widen(width, alignment).withColors(renderColors).render
    }
  }
  def equalizeHeights: ColElement = {
    val tallest = col.map(_.height).max
    ColElement(col.map(_.widen(tallest)), alignment, colors)
  }
  def divided: ColElement = {
    val divider = Element.fill(width, 1, '-', colors)
    ColElement(col.head +: col.tail.flatMap(Seq(divider, _)), alignment, colors)
  }
}

case class RowElement(row: Seq[Element],
                      alignment: VerticalAlignment = Alignment.Center,
                      colors: ColorSpec = ColorSpec(None, None)) extends Element {
  require(row.nonEmpty)
  override val width = row.map(_.width).sum
  override val height = row.map(_.height).max
  override def withColors(colors: ColorSpec): Element = copy(colors = colors)
  override def render: Seq[String] = {
    var renderedRow: Seq[String] = Seq.fill(height)("")
    row.foreach{ elt =>
      val renderColors = elt.colors.combine(colors)
      val renderedElt = elt.heighten(height, alignment).withColors(renderColors).render
      renderedRow = renderedRow.zip(renderedElt).map(t => t._1 + t._2)
    }
    renderedRow
  }
  def equalizeWidths: RowElement = {
    val widest = row.map(_.width).max
    RowElement(row.map(_.widen(widest)), alignment, colors)
  }
  def divided: RowElement = {
    val divider = Element.fill(1, height, '|', colors)
    RowElement(row.head +: row.tail.flatMap(Seq(divider, _)), alignment, colors)
  }
}

case class GridElement(rows: Seq[Seq[Element]],
                       verticalAlignment: VerticalAlignment = Alignment.Center,
                       horizontalAlignment: HorizontalAlignment = Alignment.Center,
                       colors: ColorSpec = ColorSpec(None, None)) extends Element {
  require(rows.nonEmpty)
  require(rows.map(_.size).toSet.size == 1)
  val rowToHeight: IndexedSeq[Int] = rows.indices.map(i => rows(i).map(_.height).max)
  val colToWidth: IndexedSeq[Int] = rows.head.indices.map(i => rows.map(_(i).width).max)
  override val width = colToWidth.sum
  override val height = rowToHeight.sum
  override def withColors(colors: ColorSpec): Element = copy(colors = colors)
  override def render: Seq[String] = {
    ColElement {
      (0 until rowToHeight.size).map { row =>
        RowElement {
          (0 until colToWidth.size).map { col =>
            rows(row)(col).
              heighten(rowToHeight(row), verticalAlignment).
              widen(colToWidth(col), horizontalAlignment)
          }
        }
      }
    }.render
  }
  def equalizeCells: GridElement = {
    val tallest = rowToHeight.max
    val widest = colToWidth.max
    val equalizedRows = rows.map { row =>
      row.map { cell =>
        cell.widen(widest, horizontalAlignment).heighten(tallest, verticalAlignment)
      }
    }
    this.copy(rows = equalizedRows)
  }
  def divided: Element = {
    ColElement {
      (0 until rowToHeight.size).map { row =>
        RowElement {
          (0 until colToWidth.size).map { col =>
            rows(row)(col).
              heighten(rowToHeight(row), verticalAlignment).
              widen(colToWidth(col), horizontalAlignment)
          }
        }.divided
      }
    }.divided
  }
}