package bkh.layout

trait CanLayout {
  def mkElement: Element
}
