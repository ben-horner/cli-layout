package bkh.layout

import org.scalatest.funsuite.AnyFunSuite
import org.junit.runner.RunWith
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ElementTest extends AnyFunSuite {

  test("Element should work for the simplest cases") {
    //assert(Element("").render == Seq(""))
    assert(Element("string").render == Seq("string"))
    assert(Element("string1\nstring2").render == Seq("string1", "string2"))

    val e00 = Element.fill(0, 0, 'x')
    val e01 = Element.fill(0, 1, 'x')
    val e02 = Element.fill(0, 2, 'x')
    val e03 = Element.fill(0, 3, 'x')
    val e10 = Element.fill(1, 0, 'x')
    val e11 = Element.fill(1, 1, 'x')
    val e12 = Element.fill(1, 2, 'x')
    val e13 = Element.fill(1, 3, 'x')
    val e20 = Element.fill(2, 0, 'x')
    val e21 = Element.fill(2, 1, 'x')
    val e22 = Element.fill(2, 2, 'x')
    val e31 = Element.fill(3, 1, 'x')

    assert(e00.width == 0 && e00.height == 0)
    assert(e01.width == 0 && e01.height == 1)
    assert(e02.width == 0 && e02.height == 2)
    assert(e03.width == 0 && e03.height == 3)
    assert(e10.width == 1 && e10.height == 0)
    assert(e11.render == Seq("x"))
    assert(e12.render == Seq("x", "x"))
    assert(e13.render == Seq("x", "x", "x"))
    assert(e20.width == 2 && e20.height == 0)
    assert(e21.render == Seq("xx"))
    assert(e22.render == Seq("xx", "xx"))
    assert(e31.render == Seq("xxx"))

    assert(Element.row(Seq(e00, e22)).render == Seq("xx", "xx"))
    assert(Element.row(Seq(e22, e00)).render == Seq("xx", "xx"))
    assert(Element.row(Seq(e03, e21)).render == Seq("  ", "xx", "  "))
    assert(Element.row(Seq(e21, e03)).render == Seq("  ", "xx", "  "))
    assert(Element.row(Seq(e21, e12), Alignment.Bottom).render == Seq("  x", "xxx"))
    assert(Element.row(Seq(e12, e21), Alignment.Bottom).render == Seq("x  ", "xxx"))


    assert(Element.row(Seq(e00, e02, e20, e13, e22, e21), Alignment.Center).render == Seq("  xxx  ", "  xxxxx", "  x    "))

    // |  |
    // |  |
    // |x |
    // |x |
    // |x |
    // |xx|
    // |xx|
    // |xx|
    assert(Element.col(Seq(e00, e02, e20, e13, e22, e21)).render == Seq("  ", "  ", "x ", "x ", "x ", "xx", "xx", "xx"))
    //Element.grid(Seq(Seq(), Seq()))

    assert(Element.fillRow(3, e22).render == Seq("xxxxxx", "xxxxxx"))
    assert(Element.fillCol(3, e22).render == Seq("xx", "xx", "xx", "xx", "xx", "xx"))
    //      assert(Element.fillGrid(3, 3, e22) == Seq("xxxxxx", "xxxxxx", "xxxxxx", "xxxxxx", "xxxxxx", "xxxxxx"))

    // degenerate (0 width or 0 height)
    // heighten
    // widen
    // alignments
    // colors
    // boxing and dividing
  }

}
