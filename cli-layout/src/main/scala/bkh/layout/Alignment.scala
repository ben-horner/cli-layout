package bkh.layout

sealed trait Alignment

sealed trait HorizontalAlignment
sealed trait VerticalAlignment

object Alignment {
  case object Top extends VerticalAlignment
  case object Bottom extends VerticalAlignment

  case object Left extends HorizontalAlignment
  case object Right extends HorizontalAlignment

  case object Center extends VerticalAlignment with HorizontalAlignment
}