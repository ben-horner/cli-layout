This library isn't published to any real maven repositories.

To use this library, you can clone this repository, and publish it locally, then other local projects will be able to see it.
* git clone https://gitlab.com/ben-horner/cli-layout.git
* cd cli-layout
* ./gradlew publishToMavenLocal
* import bkh.cli.layout.AnsiColors // or whatever class...

AnsiColor -- handles control strings which instruct the terminal to change colors of text (and background).
ScopedColor -- an overwritable color, such that child elements can draw their colors over parent element colors
Element -- a block of text which can be positioned with respect to other Elements
